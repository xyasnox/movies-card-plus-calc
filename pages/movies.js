import Layout from '../components/MyLayout.js';
import Movies from '../components/movie-search/Movies';

export default function About() {
  return (
    <Layout>
      <Movies/>
    </Layout>
  )
};

