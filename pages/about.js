import Layout from '../components/MyLayout.js';
import Calc from '../components/calculator/Calc';

export default function About() {
  return (
    <Layout>
      <Calc/>
    </Layout>
  )
};
