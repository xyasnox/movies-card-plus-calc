import React from 'react';
import styled from 'styled-components';

const SelectSign = styled.select`
  color: palevioletred;
  padding: 0.25em 1em;
  border: 2px solid palevioletred;
  border-radius: 3px;
`;

const Select = ({
                  onChange,
                  inputName,
                  elements,
                  value
                }) =>
  <div>
    <SelectSign onChange={e => onChange(e.target.value, inputName)}>
      {elements.map(item => <option value={item}>{item}</option>)}
    </SelectSign>
    <p>{value}</p>
  </div>;

export default Select;
