import React from 'react';
import Input from './InputForm.js';
import Select from './Select.js';
import SubmitCalc from './SubmitCalc';

export default class Calc extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      firstValue: null,
      secondValue: null,
      mathSigns: [
        {
          value: '+',
          function: this.sum,
        },
        {
          value: '-',
          function: this.sub,
        },
        {
          value: '/',
          function: this.def,
        },
        {
          value: '*',
          function: this.mul,
        },
        {
          value: '^',
          function: this.pow,
        }
      ],
      currentMathSign: '+',
      result: null
    };

    this.onChangeInput = this.onChangeInput.bind(this);
    this.calculate = this.calculate.bind(this);
  }

  onChangeInput(value, name) {
    this.setState({
      [name]: value
    })
  }

  calculate() {
    const {
      mathSigns,
      currentMathSign,
      firstValue,
      secondValue,
    } = this.state;

    this.setState({
      result: mathSigns.find(item => item.value === currentMathSign).function(firstValue, secondValue)
    });
  }

  sum(firstValue, secondValue) {
    return parseInt(firstValue) + parseInt(secondValue);
  }

  sub(firstValue, secondValue) {
    return parseInt(firstValue) - parseInt(secondValue);
  }

  def(firstValue, secondValue) {
    return parseInt(firstValue) / parseInt(secondValue);
  }

  mul(firstValue, secondValue) {
    return parseInt(firstValue) * parseInt(secondValue);
  }

  pow(firstValue, secondValue) {
    return parseInt(firstValue) ** parseInt(secondValue);
  }

  render() {
    const {
      mathSigns,
      currentMathSign,
      firstValue,
      secondValue,
      result
    } = this.state;

    return (
      <div>
        <Input onChange={this.onChangeInput} inputName="firstValue" value={firstValue} />
        <Select onChange={this.onChangeInput} inputName="currentMathSign"
                elements={mathSigns.map(item => item.value)} value={currentMathSign} />
        <Input onChange={this.onChangeInput} inputName="secondValue" value={secondValue} />
        <SubmitCalc result={result} onClick={this.calculate} />
      </div>
    )
  }
};
