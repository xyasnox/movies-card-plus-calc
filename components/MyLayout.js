import Header from './Header';
import styled from 'styled-components';

const LayoutStyled = styled.div`
  margin: 20;
  padding: 20;
  border: '1px solid #DDD'
`;

export default function Layout(props) {
  return (
    <LayoutStyled>
      <Header />
      {props.children}
    </LayoutStyled>
  )
};
