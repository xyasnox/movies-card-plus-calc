import Link from 'next/link';
import styled from 'styled-components';

const StyledLink = styled.a `
  color: green;
  font-size: 15px;
  margin-right: 15px;

  &:hover {
    color: blue;
    }
`;

export default function Header() {
  return (
    <div>
      <Link href="/">
        <StyledLink>Home</StyledLink>
      </Link>
      <Link href="/calc">
        <StyledLink>Calc</StyledLink>
      </Link>
      <Link href="/movies">
        <StyledLink>Movies</StyledLink>
      </Link>
    </div>
  )
};
